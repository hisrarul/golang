**Create and invoke a AWS Lambda function for go language**

## Download and install go package on Linux
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpmyum install gogomkdir gocd go/ls
yum -y install go zip

---

## Download the src package in a directory
echo "export GOPATH=/home/centos/go" >> ~/.bash_profile
source ~/.bash_profile
go get "github.com/aws/aws-lambda-go/lambda"

---
## Update the code in **hello.go** file
cat > hello.go
package main

import (
        "fmt"
        "github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest() {
        fmt.Print("Hello from Lambda")
}

func main() {
        lambda.Start(HandleRequest)
}

---

## Build the binary file
go build hello.go

---

## Zip the hello file and upload to the AWS Lambda function
zip hello.zip hello
